package com.jj.preparation;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Searcher {
    public static void main(String[] args) throws Exception {
        IndexReader indexReader = null;
//        try {
            String indexDir = "D:\\DevEnv\\workspace\\preparation\\src\\main\\resources\\index";
            String field = "contents";

            Path path = Paths.get(indexDir);
            Directory directory = FSDirectory.open(path);
            indexReader = DirectoryReader.open(directory);

            IndexSearcher indexSearcher = new IndexSearcher(indexReader);

            Analyzer analyzer = new StandardAnalyzer();
            QueryParser queryParser = new QueryParser(field, analyzer);
            Query query = queryParser.parse("hello");
            TopDocs topDocs = indexSearcher.search(query, 100);

            for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
                Document document = indexSearcher.doc(scoreDoc.doc);
                System.out.println(document.get(field));
            }
//        } finally {
//            indexReader.close();
//        }
    }
}
