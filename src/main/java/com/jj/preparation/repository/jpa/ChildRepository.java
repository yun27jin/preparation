package com.jj.preparation.repository.jpa;

import com.jj.preparation.model.entity.jpa.Child;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildRepository extends CrudRepository<Child, Long> {
}
