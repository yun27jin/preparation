package com.jj.preparation.repository.jpa;

import com.jj.preparation.model.entity.jpa.Parent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParentRepository extends CrudRepository<Parent, Long> {
}
