package com.jj.preparation.repository.redis;

import com.jj.preparation.model.entity.redis.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, String> {
}
