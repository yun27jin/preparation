package com.jj.preparation.service.runner;

import com.jj.preparation.model.entity.redis.Account;
import com.jj.preparation.repository.redis.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class RedisRunner implements ApplicationRunner {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        ValueOperations<String, String> values = redisTemplate.opsForValue();
        values.set("name", "name");
        values.set("framework", "spring");
        values.set("message", "hello world");

        Account account = new Account();
        account.setEmail("name@gmail.com");
        account.setName("name");

        accountRepository.save(account);

        Optional<Account> byId = accountRepository.findById(account.getId());
        System.out.println(byId.get());
    }

}
