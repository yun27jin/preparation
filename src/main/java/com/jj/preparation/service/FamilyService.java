package com.jj.preparation.service;

import com.jj.preparation.model.entity.jpa.Child;
import com.jj.preparation.model.entity.jpa.Parent;
import com.jj.preparation.repository.jpa.ChildRepository;
import com.jj.preparation.repository.jpa.ParentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FamilyService {
    @Autowired
    public FamilyService(ParentRepository parentRepository, ChildRepository childRepository) {
        this.parentRepository = parentRepository;
        this.childRepository = childRepository;
    }
    private ParentRepository parentRepository;
    private ChildRepository childRepository;

    public Parent createParent(final Parent parent) {
        return parentRepository.save(parent);
    }

    public Child createChild(final Child child) {
        return childRepository.save(child);
    }

    public Parent readParent(final Long id) {
        return parentRepository.findById(id).orElse(null);
    }

    public Child readChild(final Long id) {
        return childRepository.findById(id).orElse(null);
    }

    public Parent updateParent(final Parent p) {
        Parent parent = readParent(p.getId());
        parent.setName(p.getName());
        return parentRepository.save(parent);
    }

    public Child updateChild(final Child c) {
        Child child = readChild(c.getId());
        child.setName(c.getName());
        return childRepository.save(child);
    }

    public void deleteParent(final Long id) {
        parentRepository.deleteById(id);
    }

    public void deleteChild(final Long id) {
        childRepository.deleteById(id);
    }

}