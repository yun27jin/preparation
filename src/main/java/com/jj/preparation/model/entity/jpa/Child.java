package com.jj.preparation.model.entity.jpa;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Child implements Serializable {
    private static final long serialVersionUID = -3464404847974787877L;

    @Id
    @Column
    @NonNull
    @GeneratedValue
    private Long id;
    @Column
    @NonNull
    private String name;
    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Parent parent;
}
