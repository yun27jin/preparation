package com.jj.preparation.model.entity.redis;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("accounts")
@Data
public class Account {
    @Id
    private String id;
    private String name;
    private String email;
}
