package com.jj.preparation.model.entity.jpa;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Parent implements Serializable {
    private static final long serialVersionUID = -9125360542887009082L;

    @Id
    @Column
    @NonNull
    @GeneratedValue
    private Long id;
    @NonNull
    private String name;
}
