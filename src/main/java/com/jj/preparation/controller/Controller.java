package com.jj.preparation.controller;

import com.jj.preparation.model.entity.jpa.Child;
import com.jj.preparation.model.entity.jpa.Parent;
import com.jj.preparation.service.FamilyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {
    @Autowired
    private FamilyService familyService;

    @PostMapping("/parent")
    public Parent createParent(@RequestBody final Parent parent) {
        return familyService.createParent(parent);
    }

    @GetMapping("/parent/{id}")
    public Parent readParent(@PathVariable("id") final Long id) {
        return familyService.readParent(id);
    }

    @PutMapping("/parent")
    public Parent updateParent(@RequestBody final Parent parent) {
        return familyService.updateParent(parent);
    }

    @DeleteMapping("/parent/{id}")
    public void deleteParent(@PathVariable("id") final Long id) {
        familyService.deleteParent(id);
    }


    @PostMapping("/child")
    public Child createChild(@RequestBody final Child child) {
        return familyService.createChild(child);
    }

    @GetMapping("/child/{id}")
    public ResponseEntity<Child> readChild(@PathVariable("id") final Long id) {
        return new ResponseEntity<>(familyService.readChild(id), HttpStatus.OK);
    }

    @PutMapping("/child")
    public Child updateParent(@RequestBody final Child child) {
        return familyService.updateChild(child);
    }

    @DeleteMapping("/child/{id}")
    public void deleteChild(@PathVariable("id") final Long id) {
        familyService.deleteChild(id);
    }

}