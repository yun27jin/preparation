package com.jj.preparation;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class Indexer {

    // TODO EXCEL, TXT, PDF 등등 파일 형태에 따른 파싱 방식 확장
    public static void main(String[] args) throws Exception {
        String indexDir = "D:\\DevEnv\\workspace\\preparation\\src\\main\\resources\\index";
        String dataDir = "D:\\DevEnv\\workspace\\preparation\\src\\main\\resources\\data";
        final Path dataPath = Paths.get(dataDir);
        Directory directory = FSDirectory.open(Paths.get(indexDir));
        //Analyzer analyzer = new StandardAnalyzer();

        IndexWriterConfig indexWriterConfig = new IndexWriterConfig();
        indexWriterConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);

        IndexWriter indexWriter = new IndexWriter(directory, indexWriterConfig);

        if (Files.isDirectory(dataPath)) {
            Files.walkFileTree(dataPath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    try (InputStream stream = Files.newInputStream(file)) {
                        Document document = new Document();
                        Field pathField = new StringField("path", file.toString(), Field.Store.YES);
                        document.add(pathField);
                        document.add(new LongPoint("modified", attrs.lastModifiedTime().toMillis()));
                        document.add(new TextField("contents", new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))));
                        indexWriter.addDocument(document);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        }
        indexWriter.commit();
    }

}
