package com.jj.preparation;

import junit.framework.TestCase;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.*;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class IndexingTest {
    public String[] ids = {"1", "2"};
    public String[] unIndexed = {"Netherlands", "Italy"};
    public String[] unStored = {"Amsterdam has lots of bridges", "Venice has lots of canals"};
    public String[] text = {"Amsterdam", "Venice"};

    private Directory directory;

    @Before
    public void setUp() throws Exception {
        directory = new RAMDirectory();
        IndexWriter indexWriter = new IndexWriter(directory, new IndexWriterConfig(new WhitespaceAnalyzer()));
        for (int i = 0; i < ids.length; i++) {
            Document document = new Document();
            document.add(new StringField("id", ids[i], Field.Store.YES));
            document.add(new StringField("country", unIndexed[i], Field.Store.YES));
            document.add(new StringField("contents", unStored[i], Field.Store.NO));
            document.add(new StringField("city", text[i], Field.Store.YES));
            indexWriter.addDocument(document);
        }
        indexWriter.close();
    }

    @Test
    public void getHitCount() throws IOException {
        DirectoryReader reader = DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(reader);
        Term term = new Term("country", "I");
        Query query = new TermQuery(term);
        int hitCount = indexSearcher.count(query);
        reader.close();
        System.out.println(hitCount);
    }

}